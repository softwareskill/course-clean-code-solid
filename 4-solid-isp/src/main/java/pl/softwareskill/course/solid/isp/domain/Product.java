package pl.softwareskill.course.solid.isp.domain;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.UUID;

public interface Product {

    UUID getProductId();
    String getName();
    BigDecimal getPrice();
}
