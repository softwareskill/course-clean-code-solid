package pl.softwareskill.course.solid.isp.domain;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Book implements Product, ProductIsbn {

    UUID productId;
    String name;
    BigDecimal price;
    String isbn;
}
