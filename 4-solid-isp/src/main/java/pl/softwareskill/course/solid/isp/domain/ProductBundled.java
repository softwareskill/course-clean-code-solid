package pl.softwareskill.course.solid.isp.domain;

import java.util.Collection;

public interface ProductBundled {

    Collection<Product> getProductsInBundle();
}
