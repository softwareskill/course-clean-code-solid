package pl.softwareskill.course.solid.isp.domain;

public interface ProductIsbn {

    String getIsbn();
}
