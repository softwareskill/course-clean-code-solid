package pl.softwareskill.course.solid.isp.domain;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.UUID;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class BooksBundled implements Product, ProductBundled {

    UUID productId;
    String name;
    BigDecimal price;
    Collection<Product> productsInBundle;
}
