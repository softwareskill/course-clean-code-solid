package pl.softwareskill.course.clean.code.solid.lsp.domain;

import static pl.softwareskill.course.clean.code.solid.lsp.domain.BooksTestBuilder.CLEAN_CODE;
import static pl.softwareskill.course.clean.code.solid.lsp.domain.BooksTestBuilder.VERNON_DDD;
import pl.softwareskill.course.clean.code.solid.lsp.inmemory.PromoCodeInMemoryRepository;
import static pl.softwareskill.course.clean.code.solid.lsp.inmemory.PromoCodeInMemoryRepository.SAMPLE_PROMO_CODE_1;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class PromoCodeDiscountBasketTest {

    PromoCodeInMemoryRepository promoCodeRepository = new PromoCodeInMemoryRepository();

    @Test
    void appliesDiscountOfValidPromoCode() {
        var promoCode = SAMPLE_PROMO_CODE_1;
        var basket = givenBasket();
        var discountPolicy = givenDiscountPromoCode(promoCode.getCode());

        assertThat(basket.getFinalPrice(discountPolicy))
                .isEqualTo(basket.getItemsTotalPrice().subtract(promoCode.getDiscount()));
    }

    @Test
    void doesNotApplyDiscountForInvalidPromoCode() {
        var basket = givenBasket();
        var discountPolicy = givenDiscountPromoCode("INVALID_PROMO_CODE");

        assertThat(basket.getFinalPrice(discountPolicy))
                .isEqualTo(basket.getItemsTotalPrice());
    }

    private Basket givenBasket() {
        var basket = new Basket();

        basket.insert(CLEAN_CODE);
        basket.insert(VERNON_DDD);

        return basket;
    }

    private DiscountPolicy givenDiscountPromoCode(String promoCode) {
        return new PromoCodeDiscount(promoCode, promoCodeRepository);
    }
}



























