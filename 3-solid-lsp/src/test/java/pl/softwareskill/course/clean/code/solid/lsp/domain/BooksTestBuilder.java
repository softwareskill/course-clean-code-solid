package pl.softwareskill.course.clean.code.solid.lsp.domain;

import java.math.BigDecimal;
import java.util.UUID;

public class BooksTestBuilder {

    public static final Book VERNON_DDD = Book.builder()
            .productId(UUID.fromString("9f12b47b-81c1-4098-9fdb-21d481780a13"))
            .name("Domain-Driven Design - Vaughn Vernon")
            .price(BigDecimal.valueOf(20))
            .build();

    public static final Book CLEAN_CODE = Book.builder()
            .productId(UUID.fromString("798279b5-bf53-4b88-bf7d-cdc146e91235"))
            .name("Clean Code - Robert C. Martin")
            .price(BigDecimal.valueOf(18))
            .build();
}
