package pl.softwareskill.course.clean.code.solid.lsp.domain;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Value;

@Value(staticConstructor = "of")
@Getter
public class PromoCode {
    String code;
    BigDecimal discount;
}
