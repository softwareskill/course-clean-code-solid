package pl.softwareskill.course.clean.code.solid.lsp.domain;

import java.math.BigDecimal;

public interface DiscountPolicy {

    BigDecimal getDiscount(Basket basket);
}