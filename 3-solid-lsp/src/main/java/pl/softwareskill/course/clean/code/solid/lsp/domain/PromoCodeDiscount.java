package pl.softwareskill.course.clean.code.solid.lsp.domain;

import java.math.BigDecimal;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PromoCodeDiscount implements DiscountPolicy {

    private final String promoCode;
    private final PromoCodeRepository promoCodeRepository;

    public BigDecimal getDiscount(Basket basket) {
        return promoCodeRepository.findPromoCode(promoCode)
                .map(PromoCode::getDiscount)
                .orElse(BigDecimal.ZERO);
    }

}
