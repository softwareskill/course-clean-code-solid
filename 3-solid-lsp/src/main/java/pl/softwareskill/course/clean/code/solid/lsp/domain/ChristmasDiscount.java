package pl.softwareskill.course.clean.code.solid.lsp.domain;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDate;
import java.time.Month;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ChristmasDiscount implements DiscountPolicy {

    private final Clock clock;
    private final BigDecimal discountInPercent;

    public BigDecimal getDiscount(Basket basket) {
        if (isChristmas()) {
            return basket.getItemsTotalPrice().multiply(discountInPercent);
        }
        return BigDecimal.ZERO;
    }

    private boolean isChristmas() {
        Month currentMonth = LocalDate.now(clock).getMonth();
        return currentMonth.equals(Month.DECEMBER);
    }
}
