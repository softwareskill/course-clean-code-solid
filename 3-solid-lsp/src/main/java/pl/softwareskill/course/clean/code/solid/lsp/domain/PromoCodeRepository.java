package pl.softwareskill.course.clean.code.solid.lsp.domain;

import java.util.Optional;

public interface PromoCodeRepository {

    Optional<PromoCode> findPromoCode(String promoCode);
}
