package pl.softwareskill.course.clean.code.solid.lsp.inmemory;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import pl.softwareskill.course.clean.code.solid.lsp.domain.PromoCode;
import pl.softwareskill.course.clean.code.solid.lsp.domain.PromoCodeRepository;

public class PromoCodeInMemoryRepository implements PromoCodeRepository {

    public static final PromoCode SAMPLE_PROMO_CODE_1 = PromoCode.of("XYZ", BigDecimal.valueOf(0.1));
    public static final PromoCode SAMPLE_PROMO_CODE_2 = PromoCode.of("ABC", BigDecimal.valueOf(0.2));
    private final Set<PromoCode> codes = Set.of(SAMPLE_PROMO_CODE_1, SAMPLE_PROMO_CODE_2);

    @Override
    public Optional<PromoCode> findPromoCode(String promoCode) {
        return codes.stream()
                .filter(code -> Objects.equals(code.getCode(), promoCode))
                .findFirst();
    }
}
