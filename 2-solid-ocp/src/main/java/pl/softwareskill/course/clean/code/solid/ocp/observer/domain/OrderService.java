package pl.softwareskill.course.clean.code.solid.ocp.observer.domain;

import java.util.UUID;

public interface OrderService {

    UUID createOrder(Basket basket);
}
