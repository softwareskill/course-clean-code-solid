package pl.softwareskill.course.clean.code.solid.ocp.observer.domain;

import java.util.UUID;

public interface OrderCreatedListener {

    void onOrderCreated(UUID orderId, Basket basket);
}
