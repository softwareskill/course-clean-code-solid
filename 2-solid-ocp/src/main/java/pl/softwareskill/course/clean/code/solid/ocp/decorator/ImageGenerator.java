package pl.softwareskill.course.clean.code.solid.ocp.decorator;

public interface ImageGenerator {

    Image create();
}
