package pl.softwareskill.course.clean.code.solid.ocp.observer.domain;

import java.math.BigDecimal;

public interface DiscountPolicy {

    BigDecimal getDiscount(Basket basket);
}