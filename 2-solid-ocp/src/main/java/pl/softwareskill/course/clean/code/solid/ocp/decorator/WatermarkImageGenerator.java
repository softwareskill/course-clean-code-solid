package pl.softwareskill.course.clean.code.solid.ocp.decorator;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class WatermarkImageGenerator implements ImageGenerator {

    private final ImageGenerator delegate;

    @Override
    public Image create() {
        var image = delegate.create();
        return applyWatermark(image);
    }

    private Image applyWatermark(final Image image) {
        // add watrmark to image
        return image;
    }
}
