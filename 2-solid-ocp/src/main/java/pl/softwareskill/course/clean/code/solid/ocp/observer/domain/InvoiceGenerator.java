package pl.softwareskill.course.clean.code.solid.ocp.observer.domain;

import java.util.UUID;

public class InvoiceGenerator implements OrderCreatedListener {

    @Override
    public void onOrderCreated(UUID orderId, Basket basket) {
        // logic generate invoice
    }
}
