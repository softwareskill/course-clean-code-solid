package pl.softwareskill.course.clean.code.solid.ocp.observer.service;

import java.util.Collection;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.clean.code.solid.ocp.observer.domain.Basket;
import pl.softwareskill.course.clean.code.solid.ocp.observer.domain.OrderCreatedListener;
import pl.softwareskill.course.clean.code.solid.ocp.observer.domain.OrderService;

@RequiredArgsConstructor
public class CreateOrderService {

    private final OrderService orderService;
    private final Collection<OrderCreatedListener> listeners;

    public UUID createOrder(Basket basket) {
        var orderId = orderService.createOrder(basket);

        listeners.forEach(listener -> listener.onOrderCreated(orderId, basket));

        return orderId;
    }
}
