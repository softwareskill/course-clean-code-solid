package pl.softwareskill.course.clean.code.solid.ocp.observer.domain;

import java.util.UUID;

public class ClientNotifier implements OrderCreatedListener {

    @Override
    public void onOrderCreated(UUID orderId, Basket basket) {
        // logic for send notification to user
    }
}
