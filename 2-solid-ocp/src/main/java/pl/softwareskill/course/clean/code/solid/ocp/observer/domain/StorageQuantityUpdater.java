package pl.softwareskill.course.clean.code.solid.ocp.observer.domain;

import java.util.UUID;

public class StorageQuantityUpdater implements OrderCreatedListener {

    @Override
    public void onOrderCreated(UUID orderId, Basket basket) {
        // logic substract storage quantity
    }
}
