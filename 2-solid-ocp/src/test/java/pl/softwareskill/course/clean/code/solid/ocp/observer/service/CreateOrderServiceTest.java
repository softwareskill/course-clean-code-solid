package pl.softwareskill.course.clean.code.solid.ocp.observer.service;

import static java.util.Arrays.asList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.then;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.softwareskill.course.clean.code.solid.ocp.observer.domain.Basket;
import pl.softwareskill.course.clean.code.solid.ocp.observer.domain.OrderCreatedListener;
import pl.softwareskill.course.clean.code.solid.ocp.observer.domain.OrderService;

@ExtendWith(MockitoExtension.class)
class CreateOrderServiceTest {

    @Mock
    OrderService orderService;

    @Mock
    Basket basket;

    @Test
    void notifiesAllListeners() {
        var listeners = givenListeners();
        CreateOrderService createOrderService = new CreateOrderService(orderService, listeners);
        var orderUUID = givenOrderUUID();
        when(orderService.createOrder(any())).thenReturn(orderUUID);

        createOrderService.createOrder(basket);

        listeners.forEach(listener -> then(listener)
                .should()
                .onOrderCreated(orderUUID, basket));
    }

    private List<OrderCreatedListener> givenListeners() {
        return asList(mock(OrderCreatedListener.class), mock(OrderCreatedListener.class), mock(OrderCreatedListener.class));
    }

    UUID givenOrderUUID() {
        return UUID.fromString("5bf507b5-dfa0-4c42-bfaf-2166cb262525");
    }
}