package pl.softwareskill.course.clean.code.solid.dirty.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

public class Basket {

    private final UUID basketId;
    private final Collection<Product> products;

    public Basket() {
        this.basketId = UUID.randomUUID();
        this.products = new ArrayList<>();
    }

    public UUID getBasketId() {
        return basketId;
    }

    public void insert(Product product) {
        products.add(product);
    }

    public Collection<Product> inspect() {
        return new ArrayList<>(products);
    }

    public BigDecimal getFinalPrice() {
        BigDecimal totalPrice = inspect()
                .stream()
                .map(Product::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        Month currentMonth = LocalDate.now().getMonth();

        if (currentMonth.equals(Month.DECEMBER)) {
            // 10% christmas discount
            return totalPrice.multiply(new BigDecimal(0.9));
        }

        return totalPrice;
    }
}














