package pl.softwareskill.course.clean.code.solid.dirty.domain;

import java.math.BigDecimal;
import java.util.UUID;

public interface Product {

    UUID getProductId();
    String getName();
    BigDecimal getPrice();
}
