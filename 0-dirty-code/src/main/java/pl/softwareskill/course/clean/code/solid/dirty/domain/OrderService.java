package pl.softwareskill.course.clean.code.solid.dirty.domain;

import java.util.UUID;

public interface OrderService {

    UUID createOrder(Basket basket);
}
