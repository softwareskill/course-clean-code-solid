package pl.softwareskill.course.clean.code.solid.dirty.service;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.clean.code.solid.dirty.domain.Basket;
import pl.softwareskill.course.clean.code.solid.dirty.domain.OrderService;

@RequiredArgsConstructor
public class CreateOrderService {

    private final OrderService orderService;

    public UUID createOrder(Basket basket) {
        var orderId = orderService.createOrder(basket);

        sendEmail(orderId);
        prepareInvoice(orderId);
        updateStorageQuantity(orderId);

        return orderId;
    }

    private void sendEmail(UUID orderId) {
        // logic for send notification to user
    }

    private void prepareInvoice(UUID orderId) {
        // logic generate invoice
    }

    private void updateStorageQuantity(UUID orderId) {
        // logic substract storage quantity
    }
}
