package pl.softwareskill.course.clean.code.solid.dirty.domain;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import static pl.softwareskill.course.clean.code.solid.dirty.domain.BooksTestBuilder.CLEAN_CODE;
import static pl.softwareskill.course.clean.code.solid.dirty.domain.BooksTestBuilder.VERNON_DDD;

class BasketTest {

    @Test
    void calculatesTotalPrice() {
        Basket basket = new Basket();

        basket.insert(CLEAN_CODE);
        basket.insert(VERNON_DDD);

        assertThat(basket.getFinalPrice())
                .isEqualTo(CLEAN_CODE.getPrice().add(VERNON_DDD.getPrice()));
    }
}