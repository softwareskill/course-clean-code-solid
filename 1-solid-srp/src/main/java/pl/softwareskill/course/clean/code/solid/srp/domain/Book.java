package pl.softwareskill.course.clean.code.solid.srp.domain;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Book implements Product {

    UUID productId;
    String name;
    BigDecimal price;
}
