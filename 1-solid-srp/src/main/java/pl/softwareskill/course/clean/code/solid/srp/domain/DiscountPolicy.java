package pl.softwareskill.course.clean.code.solid.srp.domain;

import java.math.BigDecimal;

public interface DiscountPolicy {

    BigDecimal getDiscount(Basket basket);
}