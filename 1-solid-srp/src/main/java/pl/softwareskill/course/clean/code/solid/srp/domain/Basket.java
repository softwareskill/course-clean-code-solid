package pl.softwareskill.course.clean.code.solid.srp.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

public class Basket {

    private final UUID basketId;
    private final Collection<Book> products;

    public Basket() {
        this.basketId = UUID.randomUUID();
        this.products = new ArrayList<>();
    }

    public UUID getBasketId() {
        return basketId;
    }

    public void insert(Book product) {
        products.add(product);
    }

    public Collection<Book> inspect() {
        return new ArrayList<>(products);
    }

    public BigDecimal getItemsTotalPrice() {
        return inspect()
                .stream()
                .map(Product::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getFinalPrice(DiscountPolicy discountPolicy) {
        BigDecimal totalPrice = getItemsTotalPrice();
        BigDecimal discount = discountPolicy.getDiscount(this);

        return totalPrice.subtract(discount);
    }
}


























