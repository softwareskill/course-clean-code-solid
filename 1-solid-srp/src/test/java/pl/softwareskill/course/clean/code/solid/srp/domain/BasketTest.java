package pl.softwareskill.course.clean.code.solid.srp.domain;

import java.math.BigDecimal;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static pl.softwareskill.course.clean.code.solid.srp.domain.BooksTestBuilder.CLEAN_CODE;
import static pl.softwareskill.course.clean.code.solid.srp.domain.BooksTestBuilder.VERNON_DDD;

@ExtendWith(MockitoExtension.class)
class BasketTest {

    @Mock
    DiscountPolicy discountPolicy;

    @Test
    void calculatesBasketTotalPriceWithDiscount() {
        Basket basket = givenBasket();
        BigDecimal fixedDiscount = BigDecimal.valueOf(10.0);
        givenDiscount(fixedDiscount);

        assertThat(basket.getFinalPrice(discountPolicy))
                .isEqualTo(basket.getItemsTotalPrice().subtract(fixedDiscount));
    }

    private void givenDiscount(BigDecimal discount) {
        given(discountPolicy.getDiscount(any(Basket.class))).willReturn(discount);
    }

    private Basket givenBasket() {
        Basket basket = new Basket();

        basket.insert(CLEAN_CODE);
        basket.insert(VERNON_DDD);

        return basket;
    }
}