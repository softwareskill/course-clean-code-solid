package pl.softwareskill.course.clean.code.solid.srp.domain;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDate;
import java.time.Month;
import static java.time.ZoneOffset.UTC;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import static pl.softwareskill.course.clean.code.solid.srp.domain.BooksTestBuilder.CLEAN_CODE;
import static pl.softwareskill.course.clean.code.solid.srp.domain.BooksTestBuilder.VERNON_DDD;

class ChristmasDiscountTest {

    LocalDate day = null;
    BigDecimal discountInPercent = BigDecimal.valueOf(0.1);

    @Test
    void givesDiscountForChristmas() {
        givenWeHaveDay(LocalDate.of(2021, Month.DECEMBER, 24));
        DiscountPolicy discountPolicy = givenDiscountPolicy();
        Basket basket = givenBasket();

        BigDecimal discount = basket.getItemsTotalPrice().multiply(discountInPercent);
        assertThat(basket.getFinalPrice(discountPolicy))
                .isEqualTo(basket.getItemsTotalPrice().subtract(discount));
    }

    @Test
    void doesNotGiveDiscountOutOfChristmas() {
        givenWeHaveDay(LocalDate.of(2021, Month.JANUARY, 1));
        DiscountPolicy discountPolicy = givenDiscountPolicy();
        Basket basket = givenBasket();

        assertThat(basket.getFinalPrice(discountPolicy))
                .isEqualTo(basket.getItemsTotalPrice());
    }

    private DiscountPolicy givenDiscountPolicy() {
        Clock clock = Clock.fixed(day.atStartOfDay().toInstant(UTC), UTC);
        return new ChristmasDiscount(clock, discountInPercent);
    }

    private void givenWeHaveDay(LocalDate day) {
        this.day = day;
    }

    private Basket givenBasket() {
        Basket basket = new Basket();

        basket.insert(CLEAN_CODE);
        basket.insert(VERNON_DDD);

        return basket;
    }
}